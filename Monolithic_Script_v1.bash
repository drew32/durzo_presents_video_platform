#!/bin/bash
# Setup foundation server
ufw default deny incoming
ufw default allow outgoing
ufw allow ssh && ufw allow 9090
ufw allow 10000/tcp && ufw enable
nmtui
apt update && apt upgrade
apt install lxc && apt install lxd
apt install cockpit
install software-properties-common apt-transport-https wget
wget -q http://www.webmin.com/jcameron-key.asc -O- | sudo apt-key add -
add-apt-repository "deb [arch=amd64] http://download.webmin.com/download/repository sarge contrib" && apt install webmin
apt install shellinabox

lxd init

echo "source documentation https://help.ubuntu.com/lts/serverguide/lxc.html and \
https://www.cyberciti.biz/faq/install-lxd-pure-container-hypervisor-on-ubuntu-18-04-lts/"
#______________________________
#Create and launch lamp server
lxc launch ubuntu:18.04 lampy
echo "wp source docs: https://help.ubuntu.com/lts/serverguide/wordpress.html"
lxc exec lampy -- apt ufw default deny incoming && ufw default allow outgoing
lxc exec lampy -- ufw allow ssh
lxc exec lampy -- ufw allow 80 && ufw allow 443
lxc exec lampy -- ufw allow 4200/tcp
lxc exec lampy -- ufw allow 10000/tcp && ufw enable
lxc exec lampy -- nmtui
lxc exec lampy -- apt update && apt upgrade
lxc exec lampy -- apt install wordpress && apt install wget
lxc exec lampy -- cd /etc/apache2/sites-available/ && wget https://gitlab.com/drew32/durzo_presents_video_platform/raw/master/lampy/wordpress.conf
lxc exec lampy -- a2ensite wordpress && systemctl restart apache2.service
install software-properties-common apt-transport-https wget
wget -q http://www.webmin.com/jcameron-key.asc -O- | sudo apt-key add -
add-apt-repository "deb [arch=amd64] http://download.webmin.com/download/repository sarge contrib" && apt install webmin
lxc exec lampy -- apt install shellinabox
#________________________________
#Create and launch Encoder server
lxc launch ubuntu:18.04 encoder1
lxc exec encoder1 -- apt ufw default deny incoming && ufw default allow outgoing
lxc exec encoder1 -- ufw allow ssh
lxc exec encoder1 -- ufw allow 80 && ufw allow 443
lxc exec encoder1 -- ufw allow 4200/tcp
lxc exec encoder1 -- ufw allow 10000/tcp && ufw enable
lxc exec encoder1 -- nmtui
lxc exec encoder1 -- apt update && apt upgrade
lxc exec encoder1 -- git clone https://github.com/Sub-7/FFmpegUI.git && cd ~/FFmpegUI
lxc exec encoder1 -- chmod +x setup.sh && ./setup.sh
echo "random note about named pipes https://stackoverflow.com/questions/5571566/give-input-to-ffmpeg-using-named-pipes, \
also your ffmpeg web server should be working now."
# input multicast command = # # lxc exec encoder1 -- socat -u UDP4-RECV:22001,ip-add-membership=233.54.12.234:eth1 /ramdisk/fifo &
# Above command should write multicast to fifo where ffmpeg UI can pick up as a regular file.
# ideally though, the gui should be extended to include these commands https://trac.ffmpeg.org/wiki/StreamingGuide
lxc exec encoder1 -- apt install software-properties-common apt-transport-https wget
lxc exec encoder1 -- wget -q http://www.webmin.com/jcameron-key.asc -O- | sudo apt-key add -
lxc exec encoder1 -- add-apt-repository "deb [arch=amd64] http://download.webmin.com/download/repository sarge contrib" && apt install webmin
lxc exec encoder1 -- apt install shellinabox

#_________________________________
#Create and launch client server for emby, though Jellyfin is preferred as the better fork https://jellyfin.readthedocs.io/en/latest/
#lxc launch ubuntu:18.04 edge1
#lxc exec edge1 -- apt ufw default deny incoming && ufw default allow outgoing
#lxc exec edge1 -- ufw allow ssh && ufw allow 8096
#lxc exec edge1 -- ufw allow 80 && ufw allow 443
#lxc exec edge1 -- ufw allow 4200/tcp
#lxc exec edge1 -- ufw allow 10000/tcp && ufw enable
#lxc exec edge1 -- nmtui
#lxc exec edge1 -- apt update && apt upgrade
#lxc exec edge1 -- apt install git && apt install wget
#lxc exec edge1 -- wget https://github.com/MediaBrowser/Emby.Releases/releases/download/4.1.1.0/emby-server-deb_4.1.1.0_amd64.deb \
#&& dpkg -i emby-server-deb_4.1.1.0_amd64.deb
#lxc exec edge1 -- echo "Open a web browser to http://ipaddress:8096"
#lxc exec edge1 -- apt install software-properties-common apt-transport-https wget
#lxc exec edge1 -- wget -q http://www.webmin.com/jcameron-key.asc -O- | sudo apt-key add -
#lxc exec edge1 -- add-apt-repository "deb [arch=amd64] http://download.webmin.com/download/repository sarge contrib" && apt install webmin
#lxc exec edge1 -- apt install shellinabox && 

#________________________________
#Create and launch client server for Jellyfin https://jellyfin.readthedocs.io/en/latest/
lxc launch ubuntu:18.04 edge2
lxc exec edge2 -- apt ufw default deny incoming && ufw default allow outgoing
lxc exec edge2 -- ufw allow 80/tcp && ufw allow 443/tcp
lxc exec edge2 -- ufw allow 4200/tcp 1900/tcp
lxc exec edge2 -- ufw allow 8096/tcp ufw allow 8920/tcp
lxc exec edge2 -- ufw allow 10000/tcp && ufw enable
lxc exec edge2 -- nmtui
lxc exec edge2 -- apt update && apt upgrade
lxc exec edge2 -- apt install git && apt install wget
lxc exec edge2 -- apt install apt-transport-https && add-apt-repository universe
lxc exec edge2 -- wget -O - https://repo.jellyfin.org/ubuntu/jellyfin_team.gpg.key | sudo apt-key add -
lxc exec edge2 -- echo "deb [arch=$( dpkg --print-architecture )] https://repo.jellyfin.org/ubuntu $( lsb_release -c -s ) main" | sudo tee /etc/apt/sources.list.d/jellyfin.list
lxc exec edge2 -- apt update && apt install jellyfin
lxc exec edge2 -- sudo systemctl restart jellyfin && sudo systemctl enable jellyfin
lxc exec edge2 -- echo "should see the client gui at ports 8096 and 8920 now"


#__________________________________
#lxc launch ubuntu:18.04 ldap1
#lxc exec lampy -- nmtui
#lxc exec lampy -- apt update && apt upgrade
#lxc exec lampy --  apt install cockpit
#lxc exec lampy -- apt ufw default deny incoming && ufw default allow outgoing
#lxc exec lampy -- ufw allow ssh
#lxc exec lampy -- ufw allow 80 && ufw allow 443
#lxc exec lampy -- ufw enable

#____________________________________
lxc launch ubuntu:18.04 mon1
lxc exec mon1 -- apt ufw default deny incoming && ufw default allow outgoing
lxc exec mon1 -- ufw allow ssh
lxc exec mon1 -- ufw allow 80 && ufw allow 443
lxc exec mon1 -- ufw enable
lxc exec mon1 -- nmtui
lxc exec mon1 -- apt update && apt upgrade
lxc exec mon1 --  apt install cockpit
lxc exec mon1 -- apt install nginx && systemctl enable nginx.service
lxc exec mon1 -- systemctl start nginx.service && apt install mariadb-server mariadb-client
lxc exec mon1 -- systemctl start mariadb.service && systemctl enable mariadb.service
lxc exec mon1 -- mysql_secure_installation && echo " try mysql -u root -p"
lxc exec mon1 -- apt-get install software-properties-common && add-apt-repository ppa:ondrej/php
lxc exec mon1 -- apt update && apt install php7.2-fpm php7.2-common php7.2-mysql php7.2-gmp php7.2-curl php7.2-snmp php7.2-json php7.2-intl php7.2-mbstring php7.2-xmlrpc php7.2-mysql php7.2-gd php7.2-xml php7.2-cli php7.2-zip
lxc exec mon1 -- cd /etc/php/7.2/fpm/ && wget https://gitlab.com/drew32/durzo_presents_video_platform/raw/master/php.ini
lxc exec mon1 -- systemctl restart nginx.service 
lxc exec mon1 -- read -p "Enter new mysql password for librenms user : " libredbpass
lxc exec mon1 -- mysql --user="root" --password="" --execute="CREATE DATABASE 'librenms';"
lxc exec mon1 -- mysql --user="root" --password="" --database="librenms" --execute="CREATE USER 'librenmsuser'@'localhost' IDENTIFIED BY "$libredbpass"; \
GRANT ALL ON librenms.* TO 'librenmsuser'@'localhost' IDENTIFIED BY "$libredbpass" WITH GRANT OPTION; \
FLUSH PRIVILEGES; EXIT;"
lxc exec mon1 -- echo "innodb_file_per_table=1 >> /etc/mysql/mariadb.conf.d/50-server.cnf" && echo "lower_case_table_names=0" >> /etc/mysql/mariadb.conf.d/50-server.cnf
lxc exec mon1 -- apt install curl git composer fping graphviz imagemagick nmap python-memcache python-mysqldb rrdtool snmp snmpd whois
lxc exec mon1 -- curl -sS https://getcomposer.org/installer | sudo php -- --install-dir=/usr/local/bin --filename=composer
lxc exec mon1 -- useradd librenms -d /opt/librenms -M -r && usermod -a -G librenms www-data
lxc exec mon1 -- cd /opt && composer create-project --no-dev --keep-vcs librenms/librenms librenms dev-master
lxc exec mon1 -- cp /opt/librenms/snmpd.conf.example /etc/snmp/snmpd.conf && read -p "Enter new snmpstring : " snmpstring
lxc exec mon1 -- replace "RANDOMSTRINGGOESHERE" "$snmpstring" -- /etc/snmp/snmpd.conf && curl -o /usr/bin/distro https://raw.githubusercontent.com/librenms/librenms-agent/master/snmp/distro
lxc exec mon1 -- chmod +x /usr/bin/distro && systemctl restart snmpd
lxc exec mon1 -- cp /opt/librenms/librenms.nonroot.cron /etc/cron.d/librenms && cp /opt/librenms/misc/librenms.logrotate /etc/logrotate.d/librenms
lxc exec mon1 -- chown -R librenms:librenms /opt/librenms && setfacl -d -m g::rwx /opt/librenms/rrd /opt/librenms/logs /opt/librenms/bootstrap/cache/ /opt/librenms/storage/
lxc exec mon1 -- setfacl -R -m g::rwx /opt/librenms/rrd /opt/librenms/logs /opt/librenms/bootstrap/cache/ /opt/librenms/storage/ && cd /etc/nginx/sites-available/librenms
lxc exec mon1 -- wget https://gitlab.com/drew32/durzo_presents_video_platform/raw/master/librenms && ln -s /etc/nginx/sites-available/librenms /etc/nginx/sites-enabled/
lxc exec mon1 -- systemctl restart nginx.service && cd /opt/librenms
lxc exec mon1 -- ./scripts/composer_wrapper.php install --no-dev && echo "You're good to go, there is a wizart waiting for you at the librenms gui: http://yourlibrenmsserver/install.php/"

#______________________________________
#launch graylog server
lxc launch ubuntu:18.04 glog1
lxc exec glog1 --
lxc exec glog1 -- apt ufw default deny incoming && ufw default allow outgoing
lxc exec glog1 -- ufw allow ssh && ufw allow 9000/tcp
lxc exec glog1 -- ufw allow 80/tcp && ufw allow 443/tcp
lxc exec glog1 -- ufw enable
lxc exec glog1 -- nmtui
lxc exec glog1 -- apt update && apt upgrade
lxc exec glog1 -- apt-get install apt-transport-https openjdk-8-jre-headless uuid-runtime pwgen
lxc exec glog1 -- apt-get install apt-transport-https openjdk-8-jre-headless uuid-runtime pwgen
lxc exec glog1 -- apt-key adv --keyserver hkp://keyserver.ubuntu.com:80 --recv 9DA31620334BD75D9DCB49F368818C72E52529D4 && echo "deb [ arch=amd64 ] https://repo.mongodb.org/apt/ubuntu bionic/mongodb-org/4.0 multiverse" | sudo tee /etc/apt/sources.list.d/mongodb-org-4.0.list
lxc exec glog1 -- apt-get update && apt-get install -y mongodb-org
lxc exec glog1 -- systemctl daemon-reload && systemctl enable mongod.service
lxc exec glog1 -- systemctl restart mongod.service && wget -qO - https://artifacts.elastic.co/GPG-KEY-elasticsearch | sudo apt-key add -
lxc exec glog1 -- echo "deb https://artifacts.elastic.co/packages/oss-6.x/apt stable main" | sudo tee -a /etc/apt/sources.list.d/elastic-6.x.list && apt-get update && sudo apt-get install elasticsearch-oss
lxc exec glog1 -- cd /etc/elasticsearch/ && wget https://gitlab/drew32/elasticsearch.yml
lxc exec glog1 -- systemctl daemon-reload && systemctl enable elasticsearch.service
lxc exec glog1 -- systemctl restart elasticsearch.service && wget https://packages.graylog2.org/repo/packages/graylog-3.0-repository_latest.deb
lxc exec glog1 -- dpkg -i graylog-3.0-repository_latest.deb && apt-get update && sudo apt-get install graylog-server
lxc exec glog1 -- graylogvar1="echo -n "Enter Password: " && head -1 </dev/stdin | tr -d '\n' | sha256sum | cut -d" " -f1"
lxc exec glog1 -- read -p "Enter listening ip for graylog gui : " libredbpass
lxc exec glog1 -- cd /etc/graylog/server && wget https://gitlab.com/drew32/server.conf
lxc exec glog1 -- systemctl daemon-reload && systemctl enable graylog-server.service
lxc exec glog1 -- systemctl start graylog-server.service echo "should be good to go now, check the webgui at port 9000"


